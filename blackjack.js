const getDeck = function() {
const suits = ['Hearts', 'Spades', 'Clubs', 'Diamonds'];
const displayVal = [2,3,4,5,6,7,8,9,10,'Jack', 'Queen', 'King','Ace'];
const deck = new Array;
      for (var i = 0; i < displayVal.length; i++){
         for ( var x = 0; x < suits.length; x++){
            let weight = parseInt(displayVal[i]);
            if (displayVal[i]=='Jack' || displayVal[i]=='Queen' || displayVal[i]=='King'){
              weight = 10;
            }
            if (displayVal[i]=='Ace') 
              weight = 11;
            let card = { displayVal: displayVal[i], val: weight, suit: suits[x] };
            deck.push(card);
         } 
      }
       return deck;
};
console.log(getDeck());
const blackjackDeck = getDeck();
/**
 * Represents a card player (including dealer).
 * @constructor
 * @param {string} name - The name of the player
 */

const CardPlayer = function(name) {
  this.hand = [];
  this.name = name;
  return this;
};

CardPlayer.prototype.drawCard = function drawCard() {
  const randomCard = blackjackDeck[Math.floor(Math.random()*blackjackDeck.length)];
  this.hand.push(randomCard);
};

//////
// CREATE TWO NEW CardPlayers

let dealer = new CardPlayer('Dealer'); // TODO
let player = new CardPlayer('Player'); // TODO

console.log(dealer);
console.log(player);


/**
 * Calculates the score of a Blackjack hand
 * @param {Array} hand - Array of card objects with val, displayVal, suit properties
 * @returns {Object} - Object containing Total points and whether hand isSoft
 */
const calcPoints = function(hand) {
  
  let total = 0;
  let numberOfAces = 0;
  let numberOfLowAces = 0;
  
  for (let i = 0; i < hand.length; i++) {
    const card = hand[i]; 
    total += card.val;
    if (card.displayVal === 'Ace') {
      numberOfAces++;
    }
  };

  for (let i = numberOfAces; i > 0; i--)  {
    if (total > 21) {
      total -= 10;
      numberOfLowAces++;
    }
  }

  let isSoft = false;
  if (numberOfAces > 0 && numberOfAces !== numberOfLowAces) {
    isSoft = true;
  }

  return {
    total,
    isSoft
  }
}

// calcPoints(hand);

/**
 * Determines whether the dealer should draw another card
 * @param {Array} dealerHand Array of card objects with val, displayVal, suit properties
 * @returns {boolean} whether dealer should draw another card
 */
const dealerShouldDraw = function(dealerHand) {
  // CREATE FUNCTION HERE
  const dealerPoints = calcPoints(dealerHand).total;
  let isSoft = false;
  if (dealerPoints < 17) {
    return true;
  } else if (dealerPoints == 17 && isSoft) {
    return false;
  }
}

/**
 * Determines the winner if both player and dealer stand
 * @param {number} playerScore 
 * @param {number} dealerScore 
 * @returns {string} States the player's score, the dealer's score, and who wins
 */
const determineWinner = function(playerScore, dealerScore) {
  // CREATE FUNCTION HERE
  if (playerScore == dealerScore){
      return `Dealer Score: ${dealerScore}, Player Score: ${playerScore} Tie.`; 
    } else if (playerScore > 21) {
      return `Dealer Score: ${dealerScore}, Player Score: ${playerScore}. Dealer wins.`; 
    } else if (dealerScore > 21) {
      return `Dealer Score: ${dealerScore}, Player Score: ${playerScore}. Player wins.`; 
    } else if (playerScore > dealerScore) {
      return `Dealer Score: ${dealerScore}, Player Score: ${playerScore}. Player wins.`; 
    }else {
      return `Dealer Score: ${dealerScore}, Player Score: ${playerScore}. Dealer wins.`; 
    }
}

/**
 * Creates user prompt to ask if they'd like to draw a card
 * @param {number} count 
 * @param {string} dealerCard 
 */
const getMessage = function(count, dealerCard) {
  return `Dealer showing ${dealerCard.displayVal}, your count is ${count}.  Draw card?`
}

/**
 * Logs the player's hand to the console
 * @param {CardPlayer} player 
 */
const showHand = function(player) {
  let displayHand = player.hand.map(function(card) { return card.displayVal});
  console.log(`${player.name} hand is ${displayHand.join(', ')} (${calcPoints(player.hand).total})`);
}

/**
 * Runs Blackjack Game
 */
const startGame = function() {
  player.drawCard();
  dealer.drawCard();
  player.drawCard();
  dealer.drawCard();

  let playerScore = calcPoints(player.hand).total;
  showHand(player);
  while (playerScore < 21 && confirm(getMessage(playerScore, dealer.hand[0]))) {
    player.drawCard();
    playerScore = calcPoints(player.hand).total;
    showHand(player);
  }
  if (playerScore > 21) {
    return 'You went over 21 - you lose!';
  }
  console.log(`Player stands at ${playerScore}`);

  // IMPLEMENT DEALER LOGIC BELOW
  
if (dealerShouldDraw(dealer.hand)) {
      dealer.drawCard();
      showHand(dealer);
    }

  playerScore = calcPoints(player.hand).total;
  dealerScore = calcPoints(dealer.hand).total;

  return determineWinner(playerScore, dealerScore);
}
console.log(startGame());