/**
 * Returns an array of 52 Cards
 * @returns {Array} deck - a deck of cards
 */


  const getDeck = function() {
    const suits = ['Hearts', 'Spades', 'Clubs', 'Diamonds'];
	const displayVal = [2,3,4,5,6,7,8,9,10,'Jack', 'Queen', 'King','Ace'];
	const deck = new Array;
      for (var i = 0; i < displayVal.length; i++){
         for ( var x = 0; x < suits.length; x++){
            let weight = parseInt(displayVal[i]);
            if (displayVal[i]=='Jack' || displayVal[i]=='Queen' || displayVal[i]=='King')
              weight = 10;
            if (displayVal[i]=='Ace')
              weight = 11;
            let card = { displayVal: displayVal[i], val: weight, suit: suits[x] };
            deck.push(card);
         } 
      }
       return deck;
  }

// CHECKS
const deck = getDeck();
console.log(`Deck length equals 52? ${deck.length === 52}`);

const randomCard = deck[Math.floor(Math.random() * 52)];

const cardHasVal = randomCard && randomCard.val && typeof randomCard.val === 'number';
console.log(`Random card has val? ${cardHasVal}`);

const cardHasSuit = randomCard && randomCard.suit && typeof randomCard.suit === 'string';
console.log(`Random card has suit? ${cardHasSuit}`);

const cardHasDisplayVal = randomCard && 
    randomCard.displayVal && 
    typeof randomCard.displayVal === 'string';
console.log(`Random card has display value? ${cardHasDisplayVal}`);